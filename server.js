const redis = require("redis");
//var createClient = require('then-redis').createClient
const client = redis.createClient({
    host: "127.0.0.1",
    db: 0
});

const request = require('request-promise');
const sleep = require('sleep');
//const querystring =  require('querystring');

function parseData(data){
    const data_parse = {};
    data = JSON.parse(data);
    data_parse.bus = data.bus;
    data_parse.gps = "Point ("+data.latitude+" "+data.longitude+")";
    data_parse.timestamp = data.inserted;
    data_parse.speed = data.speed;
    data_parse.orientation = data.orientation;
    data_parse.mileage = data.mileage;
    data_parse.from_cmd = data.from_cmd;
    data_parse.request_id = data.request_id;
    return data_parse;
}

function doRequest(accept, reject, data){
    console.log("doRequest");
    request.post({
        uri: 'http://localhost:8000/api/bus/location/',
        body: parseData(data),
        json: true
    })
    .then(function(response, body){
        if ((response.status == 200 || response.status == 201)) {
            console.log('dato enviado');
            //console.log(body)
            accept();
        }
        else {
            //console.log(error);
            reject("", data);
        }
    })
    .catch(function(err) {
      console.log("----Error----");
      console.log(err);
      console.log("----Fin----");
      reject({err, data});
    })
}

function getData(){
    try{
        const pr = new Promise((resolve, reject)=>{
            client.lpop("pings", function(err, result){
                console.log(JSON.parse(result));
                if(result){
                    doRequest(resolve, reject, result);
                }else{
                    console.log('dormir 5');
                    sleep.sleep(5);
                    resolve();
                }
            });
        }).then(function(){
            console.log("call again");
            getData();
        }).catch(function(response) {
            const {data} = response;
            console.log(data);
            //client.lpush("pings", data);
            getData();
        });
    }catch(err){
        console.log("----Error----");
        console.log(err);
        console.log("----Fin----");
    }
}
//client.on("error", function (err) {
//    console.log("Error console");
//});

client.on('connect', function() {
    console.log('connected');
    getData();
});
